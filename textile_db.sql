drop database textile_db;
create database textile_db;
use textile_db;

CREATE TABLE customers (
  id MEDIUMINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nama VARCHAR(50) NOT NULL,
  alamat VARCHAR(100) NOT NULL,
  telp VARCHAR(30) NOT NULL,
  ktp VARCHAR(30) NOT NULL,
  status VARCHAR(30),
  npwp VARCHAR(30) NOT NULL
) ENGINE = InnoDB;

CREATE TABLE purchases (
  no_pq VARCHAR(200) NOT NULL PRIMARY KEY,
  no_po VARCHAR(200),
  warna VARCHAR(30),
  jenis_bahan VARCHAR(30),
  jumlah VARCHAR(30),
  harga VARCHAR(30),
  status VARCHAR(30),
  customer_id MEDIUMINT NOT NULL,
  CONSTRAINT `fk_purchase_customer`
    FOREIGN KEY (customer_id) REFERENCES customers (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE = InnoDB;