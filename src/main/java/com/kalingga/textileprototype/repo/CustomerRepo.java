package com.kalingga.textileprototype.repo;

import com.kalingga.textileprototype.dom.Customer;
import com.kalingga.textileprototype.exception.BadRequestException;
import com.kalingga.textileprototype.exception.NotFoundException;

import java.util.List;

public interface CustomerRepo {
    List<Customer> findAll(String nama) throws NotFoundException;

    Customer findById(Integer customerId) throws NotFoundException;

    void create(String nama, String alamat, String telp, String ktp, String npwp) throws BadRequestException;

    void update(Integer customerId, Customer customer) throws BadRequestException;

    void deleteById(Integer customerId);

    Integer customerPurchaseCount(Integer customerId) throws NotFoundException;
}
