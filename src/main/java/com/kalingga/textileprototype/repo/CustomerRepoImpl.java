package com.kalingga.textileprototype.repo;

import com.kalingga.textileprototype.dom.Customer;
import com.kalingga.textileprototype.exception.BadRequestException;
import com.kalingga.textileprototype.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class CustomerRepoImpl implements CustomerRepo{

    private static final String SQL_FIND_ALL = "SELECT * FROM customers WHERE status NOT IN ('DELETED')";
    private static final String SQL_FIND_ALL_BY_NAME = "SELECT * FROM customers WHERE nama LIKE ? AND status NOT IN ('DELETED')";
    private static final String SQL_FIND_BY_ID = "SELECT * FROM customers WHERE id = ? AND status NOT IN ('DELETED')";
    private static final String SQL_CREATE = "INSERT INTO customers (nama, alamat, telp, ktp, npwp, status) " +
            "VALUES(?, ?, ?, ?, ?, 'CREATED')";
    private static final String SQL_UPDATE = "UPDATE customers SET nama = ?, alamat = ?, telp = ?, ktp = ?, npwp = ? " +
            "WHERE id = ? AND status NOT IN ('DELETED')";
//    private static final String SQL_DELETE = "DELETE FROM customers WHERE id = ?";
    private static final String SQL_DELETE = "UPDATE customers SET status = 'DELETED' WHERE id = ?";
    private static final String SQL_DELETE_VALIDATION = "SELECT COUNT(*) AS COUNT FROM purchases WHERE customer_id = ? AND status NOT IN ('DELETED')";

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List<Customer> findAll(String nama) throws NotFoundException {
        if (nama != "")
            return jdbcTemplate.query(SQL_FIND_ALL_BY_NAME, new Object[]{"%" + nama + "%"}, customerRowMapper);
        return jdbcTemplate.query(SQL_FIND_ALL, new Object[]{}, customerRowMapper);
    }

    @Override
    public Customer findById(Integer customerId) throws NotFoundException {
        try {
            return jdbcTemplate.queryForObject(SQL_FIND_BY_ID, new Object[]{customerId}, customerRowMapper);
        }catch (Exception e) {
            throw new NotFoundException("Customer not found");
        }
    }

    @Override
    public void create(String nama, String alamat, String telp, String ktp, String npwp) throws BadRequestException {
        try {
            jdbcTemplate.update(SQL_CREATE, new Object[]{nama, alamat, telp, ktp, npwp});
        }catch (Exception e) {
            throw new BadRequestException("Invalid request");
        }
    }

    @Override
    public void update(Integer customerId, Customer customer) throws BadRequestException {
        try {
            jdbcTemplate.update(SQL_UPDATE, new Object[]{customer.getNama(), customer.getAlamat(), customer.getTelp(), customer.getKtp(), customer.getNpwp(), customerId});
        }catch (Exception e) {
            throw new BadRequestException("Invalid request");
        }
    }

    @Override
    public void deleteById(Integer customerId) {
        jdbcTemplate.update(SQL_DELETE, new Object[]{customerId});
    }

    @Override
    public Integer customerPurchaseCount(Integer customerId) throws NotFoundException {
        return jdbcTemplate.queryForObject(SQL_DELETE_VALIDATION, new Object[]{customerId}, Integer.class);
    }

    private RowMapper<Customer> customerRowMapper = ((rs, rowNum) -> {
        return new Customer(rs.getInt("ID"),
                rs.getString("NAMA"),
                rs.getString("ALAMAT"),
                rs.getString("TELP"),
                rs.getString("KTP"),
                rs.getString("NPWP"));
    });
}
