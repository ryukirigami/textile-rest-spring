package com.kalingga.textileprototype.repo;

import com.kalingga.textileprototype.dom.Customer;
import com.kalingga.textileprototype.dom.Purchase;
import com.kalingga.textileprototype.exception.BadRequestException;
import com.kalingga.textileprototype.exception.NotFoundException;

import java.util.List;

public interface PurchaseRepo {
    Integer getCount() throws NotFoundException;

    List<Purchase> findAll() throws NotFoundException;

    List<Purchase> findAllPq() throws NotFoundException;

    List<Purchase> findAllPo() throws NotFoundException;

    Purchase findByPQ(String noPq) throws NotFoundException;

    void create(String noPq, String warna, String jenisBahan, String jumlah, String harga, String status, Integer customerId) throws BadRequestException;

    void update(String noPq, Purchase purchase) throws BadRequestException;

    void deleteByPq(String noPq);
}
