package com.kalingga.textileprototype.repo;

import com.kalingga.textileprototype.dom.Customer;
import com.kalingga.textileprototype.dom.Purchase;
import com.kalingga.textileprototype.exception.BadRequestException;
import com.kalingga.textileprototype.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PurchaseRepoImpl implements PurchaseRepo{

    private static final String SQL_COUNT = "SELECT COUNT(*) AS COUNT FROM purchases";
    private static final String SQL_FIND_ALL = "SELECT P.*, C.nama FROM purchases P INNER JOIN customers C ON P.customer_id = C.id";
    private static final String SQL_FIND_ALL_PQ = "SELECT P.*, C.nama FROM purchases P INNER JOIN customers C ON P.customer_id = C.id WHERE P.status = 'ON_QUOTATION'"; //"SELECT * FROM purchases WHERE status = 'ON_QUOTATION'";
    private static final String SQL_FIND_ALL_PO = "SELECT P.*, C.nama FROM purchases P INNER JOIN customers C ON P.customer_id = C.id WHERE P.status = 'ON_ORDER'"; //"SELECT * FROM purchases WHERE status = 'OB_ORDER'";
    private static final String SQL_FIND_BY_PQ = "SELECT P.*, C.nama FROM purchases P INNER JOIN customers C ON P.customer_id = C.id WHERE P.no_pq = ?";
    private static final String SQL_CREATE = "INSERT INTO purchases (no_pq, warna, jenis_bahan, jumlah, harga, status, " +
            "customer_id) VALUES(?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE = "UPDATE purchases SET warna = ?, jenis_bahan = ?, jumlah = ?, harga = ?, status = ?," +
            " no_po = ? WHERE no_pq = ?";
//    private static final String SQL_DELETE = "DELETE FROM purchases WHERE no_pq = ?";
    private static final String SQL_DELETE = "UPDATE purchases SET status = 'DELETED' WHERE no_pq = ?";

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public Integer getCount() throws NotFoundException {
        return jdbcTemplate.queryForObject(SQL_COUNT, new Object[]{}, Integer.class);
    }

    @Override
    public List<Purchase> findAll() throws NotFoundException {
        return jdbcTemplate.query(SQL_FIND_ALL, new Object[]{}, purchaseRowMapper);
    }

    @Override
    public List<Purchase> findAllPq() throws NotFoundException {
        return jdbcTemplate.query(SQL_FIND_ALL_PQ, new Object[]{}, purchaseRowMapper);
    }

    @Override
    public List<Purchase> findAllPo() throws NotFoundException {
        return jdbcTemplate.query(SQL_FIND_ALL_PO, new Object[]{}, purchaseRowMapper);
    }

    @Override
    public Purchase findByPQ(String noPq) throws NotFoundException {
        try {
            return jdbcTemplate.queryForObject(SQL_FIND_BY_PQ, new Object[]{noPq}, purchaseRowMapper);
        }catch (Exception e) {
            throw new NotFoundException("Purchase not found");
        }
    }

    @Override
    public void create(String noPq, String warna, String jenisBahan, String jumlah, String harga, String status, Integer customerId) throws BadRequestException {
        try {
            jdbcTemplate.update(SQL_CREATE, new Object[]{noPq, warna, jenisBahan, jumlah, harga, "ON_QUOTATION", customerId});
        }catch (Exception e) {
            throw new BadRequestException("Invalid request");
        }
    }

    @Override
    public void update(String noPq, Purchase purchase) throws BadRequestException {
        try {
            jdbcTemplate.update(SQL_UPDATE, new Object[]{purchase.getWarna(), purchase.getJenisBahan(),
                    purchase.getJumlah(), purchase.getHarga(), purchase.getStatus(), purchase.getNoPo(), noPq });
        }catch (Exception e) {
            throw new BadRequestException("Invalid request");
        }
    }

    @Override
    public void deleteByPq(String noPq) {
        jdbcTemplate.update(SQL_DELETE, new Object[]{noPq});
    }

    private RowMapper<Purchase> purchaseRowMapper = ((rs, rowNum) -> {
//        Purchase purchase = new Purchase(rs.getString("noPq"),
//                rs.getString("noPo"),
//                rs.getString("warna"),
//                rs.getString("jenisBahan"),
//                rs.getString("jumlah"),
//                rs.getString("harga"),
//                rs.getString("status"),
//                rs.getInt("customerId"));
//        purchase.setCustomerName(rs.getString("nama"));
        Purchase purchase = new Purchase(rs.getString("NO_PQ"),
                rs.getString("NO_PO"),
                rs.getString("WARNA"),
                rs.getString("JENIS_BAHAN"),
                rs.getString("JUMLAH"),
                rs.getString("HARGA"),
                rs.getString("STATUS"),
                rs.getInt("CUSTOMER_ID"));
        purchase.setCustomerName(rs.getString("NAMA"));
        return purchase;
    });
}
