package com.kalingga.textileprototype.service;

import com.kalingga.textileprototype.dom.Purchase;
import com.kalingga.textileprototype.exception.BadRequestException;
import com.kalingga.textileprototype.exception.NotFoundException;

import java.util.List;

public interface PurchaseService {
    Integer getPurchaseCount();

    List<Purchase> fetchAllPurchase();

    List<Purchase> fetchAllPq();

    List<Purchase> fetchAllPo();

    Purchase fetchPurchaseByPq(String noPq) throws NotFoundException;

    void addPurchase(String noPq, String warna, String jenisBahan, String jumlah, String harga, String status, Integer customerId) throws NotFoundException;

    void updatePurchase(String noPq, Purchase purchase) throws BadRequestException;

    void deletePurchase(String noPq) throws BadRequestException;
}
