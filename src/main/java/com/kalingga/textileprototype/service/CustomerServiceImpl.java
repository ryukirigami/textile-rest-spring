package com.kalingga.textileprototype.service;

import com.kalingga.textileprototype.dom.Customer;
import com.kalingga.textileprototype.exception.BadRequestException;
import com.kalingga.textileprototype.exception.NotFoundException;
import com.kalingga.textileprototype.repo.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService{

    @Autowired
    CustomerRepo customerRepo;

    @Override
    public List<Customer> fetchAllCustomers(String nama) {
        return customerRepo.findAll(nama);
    }

    @Override
    public Customer fetchCustomerById(Integer customerId) throws NotFoundException {
        return customerRepo.findById(customerId);
    }

    @Override
    public void addCustomer(String nama, String alamat, String telp, String ktp, String npwp) throws NotFoundException {
        customerRepo.create(nama, alamat, telp, ktp, npwp);
    }

    @Override
    public void updateCustomer(Integer customerId, Customer customer) throws BadRequestException {
        customerRepo.update(customerId, customer);
    }

    @Override
    public void deleteCustomer(Integer customerId) throws BadRequestException {
        this.fetchCustomerById(customerId);
        customerRepo.deleteById(customerId);
    }

    @Override
    public Integer getCustomerPurchaseCount(Integer customerId) {
        return customerRepo.customerPurchaseCount(customerId);
    }
}
