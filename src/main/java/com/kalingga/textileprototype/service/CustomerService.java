package com.kalingga.textileprototype.service;

import com.kalingga.textileprototype.dom.Customer;
import com.kalingga.textileprototype.exception.BadRequestException;
import com.kalingga.textileprototype.exception.NotFoundException;

import java.util.List;

public interface CustomerService {
    List<Customer> fetchAllCustomers(String nama);

    Customer fetchCustomerById(Integer customerId) throws NotFoundException;

    void addCustomer(String nama, String alamat, String telp, String ktp, String npwp) throws NotFoundException;

    void updateCustomer(Integer customerId, Customer customer) throws BadRequestException;

    void deleteCustomer(Integer customerId) throws BadRequestException;

    Integer getCustomerPurchaseCount(Integer customerId);
}
