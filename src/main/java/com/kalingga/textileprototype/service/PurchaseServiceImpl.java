package com.kalingga.textileprototype.service;

import com.kalingga.textileprototype.dom.Customer;
import com.kalingga.textileprototype.dom.Purchase;
import com.kalingga.textileprototype.exception.BadRequestException;
import com.kalingga.textileprototype.exception.NotFoundException;
import com.kalingga.textileprototype.repo.CustomerRepo;
import com.kalingga.textileprototype.repo.PurchaseRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PurchaseServiceImpl implements PurchaseService{

    @Autowired
    PurchaseRepo purchaseRepo;

    @Override
    public Integer getPurchaseCount() {
        return purchaseRepo.getCount();
    }

    @Override
    public List<Purchase> fetchAllPurchase() {
        return purchaseRepo.findAll();
    }

    @Override
    public List<Purchase> fetchAllPq() {
        return purchaseRepo.findAllPq();
    }

    @Override
    public List<Purchase> fetchAllPo() {
        return purchaseRepo.findAllPo();
    }

    @Override
    public Purchase fetchPurchaseByPq(String noPq) throws NotFoundException {
        return purchaseRepo.findByPQ(noPq);
    }

    @Override
    public void addPurchase(String noPq, String warna, String jenisBahan, String jumlah, String harga, String status, Integer customerId) throws NotFoundException {
        purchaseRepo.create(noPq, warna, jenisBahan, jumlah, harga, status, customerId);
    }

    @Override
    public void updatePurchase(String noPq, Purchase purchase) throws BadRequestException {
        purchaseRepo.update(noPq, purchase);
    }

    @Override
    public void deletePurchase(String noPq) throws BadRequestException {
        this.fetchPurchaseByPq(noPq);
        purchaseRepo.deleteByPq(noPq);
    }
}
