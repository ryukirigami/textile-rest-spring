package com.kalingga.textileprototype.dom;

public class Purchase {
    private String noPq;
    private String noPo;
    private String warna;
    private String jenisBahan;
    private String jumlah;
    private String harga;
    private String status;
    private Integer customerId;
    private String customerName;

    public Purchase(String noPq, String noPo, String warna, String jenisBahan, String jumlah, String harga,  String status, Integer customerId) {
        this.noPq = noPq;
        this.noPo = noPo;
        this.warna = warna;
        this.jenisBahan = jenisBahan;
        this.jumlah = jumlah;
        this.harga = harga;
        this.status = status;
        this.customerId = customerId;
    }

    public String getNoPq() {
        return noPq;
    }

    public void setNoPq(String noPq) {
        this.noPq = noPq;
    }

    public String getNoPo() {
        return noPo;
    }

    public void setNoPo(String noPo) {
        this.noPo = noPo;
    }

    public String getWarna() {
        return warna;
    }

    public void setWarna(String warna) {
        this.warna = warna;
    }

    public String getJenisBahan() {
        return jenisBahan;
    }

    public void setJenisBahan(String jenisBahan) {
        this.jenisBahan = jenisBahan;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
}
