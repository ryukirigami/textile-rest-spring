package com.kalingga.textileprototype.source;

import com.kalingga.textileprototype.dom.Customer;
import com.kalingga.textileprototype.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/customer")
public class CustomerSource {

    @Autowired
    CustomerService customerService;

    @GetMapping("")
    public ResponseEntity<List<Customer>> getAllCustomers(HttpServletRequest request) {
        String nama = "";
        if (request.getQueryString() != null) nama = request.getParameter("nama");
        List<Customer> categories = customerService.fetchAllCustomers(nama);
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Customer> getCustomerById(HttpServletRequest request,
                                                    @PathVariable("id") Integer customerId) {
        Customer customer = customerService.fetchCustomerById(customerId);
        return new ResponseEntity<>(customer, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<Map<String, Boolean>> addCustomer(HttpServletRequest request,
                                                @RequestBody Map<String, Object> customerMap) {
        String nama = (String) customerMap.get("nama");
        String alamat = (String) customerMap.get("alamat");
        String telp = (String) customerMap.get("telp");
        String ktp = (String) customerMap.get("ktp");
        String npwp = (String) customerMap.get("npwp");
        customerService.addCustomer(nama, alamat, telp, ktp, npwp);
        Map<String, Boolean> map = new HashMap<>();
        map.put("success", true);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Map<String, Boolean>> updateCustomer(HttpServletRequest request,
                                                               @PathVariable("id") Integer customerId,
                                                               @RequestBody Customer customer) {
        customerService.updateCustomer(customerId, customer);
        Map<String, Boolean> map = new HashMap<>();
        map.put("success", true);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteCustomer(HttpServletRequest request,
                                                               @PathVariable("id") Integer customerId) {

        if (customerService.getCustomerPurchaseCount(customerId) == 0) {
            customerService.deleteCustomer(customerId);
            Map<String, Boolean> map = new HashMap<>();
            map.put("success", true);
            return new ResponseEntity<>(map, HttpStatus.OK);
        } else {
            Map<String, Boolean> map = new HashMap<>();
            map.put("conflict", true);
            return new ResponseEntity<>(map, HttpStatus.OK);
        }
    }

//    @DeleteMapping("/{id}")
//    public ResponseEntity<Map<String, Boolean>> deleteCustomerConfirm(HttpServletRequest request,
//                                                               @PathVariable("id") Integer customerId) {
//
//        customerService.getCustomerPurchaseCount(customerId);
//        Map<String, Boolean> map = new HashMap<>();
//        map.put("success", true);
//        return new ResponseEntity<>(map, HttpStatus.OK);
//    }
}
