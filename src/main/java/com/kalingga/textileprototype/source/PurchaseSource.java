package com.kalingga.textileprototype.source;

import com.kalingga.textileprototype.dom.Purchase;
import com.kalingga.textileprototype.service.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/api/purchase")
public class PurchaseSource {

    @Autowired
    PurchaseService purchaseService;

    @GetMapping("")
    public ResponseEntity<List<Purchase>> getAllPurchases(HttpServletRequest request) {
        List<Purchase> categories = purchaseService.fetchAllPurchase();
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @GetMapping("/pq")
    public ResponseEntity<List<Purchase>> getAllPq(HttpServletRequest request) {
        List<Purchase> categories = purchaseService.fetchAllPq();
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @GetMapping("/po")
    public ResponseEntity<List<Purchase>> getAllPo(HttpServletRequest request) {
        List<Purchase> categories = purchaseService.fetchAllPo();
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Purchase> getPurchaseByPq(HttpServletRequest request,
                                                    @PathVariable("id") String noPq) {
        Purchase purchase = purchaseService.fetchPurchaseByPq(noPq);
        return new ResponseEntity<>(purchase, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<Map<String, Boolean>> addPurchase(HttpServletRequest request,
                                                            @RequestBody Map<String, Object> purchaseMap) {
//        String noPq = (String) purchaseMap.get("noPq");
        String noPq = new StringBuilder("PQ").append(String.format("%06d", purchaseService.getPurchaseCount() + 1)).toString();
        String warna = (String) purchaseMap.get("warna");
        String jenisBahan = (String) purchaseMap.get("jenisBahan");
        String jumlah = (String) purchaseMap.get("jumlah");
        String harga = (String) purchaseMap.get("harga");
        String status = (String) purchaseMap.get("status");
        Integer customerId = (Integer) purchaseMap.get("customer");
        purchaseService.addPurchase(noPq, warna, jenisBahan, jumlah, harga, status, customerId);
        Map<String, Boolean> map = new HashMap<>();
        map.put("success", true);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Map<String, Boolean>> updatePurchase(HttpServletRequest request,
                                                               @PathVariable("id") String noPq,
                                                               @RequestBody Purchase purchase) {
        purchaseService.updatePurchase(noPq, purchase);
        Map<String, Boolean> map = new HashMap<>();
        map.put("success", true);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Boolean>> deletePurchase(HttpServletRequest request,
                                                               @PathVariable("id") String noPq) {
        purchaseService.deletePurchase(noPq);
        Map<String, Boolean> map = new HashMap<>();
        map.put("success", true);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }
}
