# Textile Rest Spring

This project is used for Textile CMS Prototype

## Configuration

```
    - Install MySQL
    - Restore textile_db.sql
    - Install IDE Intellij Community Edition
    - Open from from existing folder 
    - Note (dengan Intellij IDE secara otomastis akan mendownload semua dependency yang dibutuhkan, tunggu proses download selesai sebelum meng-compile)
```
## Run Project

Run `./mvnw spring-boot:run`

## Further help

Navigate to `http://localhost:8080/api/`
